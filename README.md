# Assignment Streams - Digital Route
Application for collecting and aggregating data from electric scooters.

## Dependencies
[NodeJS](https://nodejs.org) (v8 or above)

## Setup
Install dependencies
```
npm install
```

Run tests
```
npm test
```

Run application
```
npm start
```


## Notes

#### Cost format
Due to the way decimal calculations are handled in javascript, I chose to use the ISO4217 currency format when calculating the costs for the scooters.
https://en.wikipedia.org/wiki/ISO_4217

#### Caching
In the rates.js I added a caching mechanism for when fetching the rates using a local cache. In a production environment I would instead implement a centralised caching mechanism using redis in the case this application would be deployed as a worker that had to scale horizontally.
