const { Transform } = require('stream');
const moment = require('moment');
const log = require('./logging');

const {
  formatToISO4217,
  formatISO4217ToDecimals,
} = require('./currency');

const { getRates } = require('./rates');

class Aggregator extends Transform {
  constructor(options) {
    super(Object.assign({}, { objectMode: true }, options));
    this.aggregates = [];
  }

  async _transform(data, encoding, done) {
    try {
      const dateFormat = 'YYYY-MM-DD HH:mm:ss';
      const start = moment(data.startTime, dateFormat);
      const end = moment(data.endTime, dateFormat);
      const minutes = moment.duration(end.diff(start)).asMinutes();
      let record = this.aggregates.find((r => r.customerId === data.customerId));
      const durationMinutes = Math.ceil(minutes);

      const ratesObj = await getRates();
      const rate = ratesObj[data.zone];
      let cost;
      if (!rate) {
        log.error(`Unable to find rates for zone: ${data.zone}`);
      } else {
        /* Using ISO4217 in order to do proper cost calculations */
        const costISO4217 = durationMinutes * formatToISO4217(rate.price, rate.currency);
        const costDecimal = formatISO4217ToDecimals(costISO4217, rate.currency);
        cost = {
          costISO4217,
          costDecimal,
          currency: rate.currency,
          zone: rate.zone,
        };
      }

      if (record) {
        record.totalRides += 1;
        record.durationMinutes += durationMinutes;
        if (cost) {
          record.costs.push(cost);
        }
      } else {
        record = {
          customerId: data.customerId,
          totalRides: 1,
          durationMinutes,
          costs: [],
        };
        if (cost) {
          record.costs.push(cost);
        }
        this.aggregates.push(record);
      }
      done();
    } catch (e) {
      throw e;
    }
  }

  _flush(done) {
    this.push(this.aggregates.map((v) => {
      /* Calculate the total cost in all zones for each currency */
      const value = v;
      value.totalCostsISO4217 = {};
      value.costs.map((cost) => {
        const { currency } = cost;
        if (!value.totalCostsISO4217[currency]) {
          value.totalCostsISO4217[currency] = 0;
        }
        value.totalCostsISO4217[currency] += cost.costISO4217;
        return value;
      });
      return value;
    }));
    done();
  }
}

module.exports = Aggregator;
