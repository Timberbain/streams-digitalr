const request = require('request');
const csv = require('fast-csv');

const SchemaValidator = require('./schema_validator');
const JSONFileWriter = require('./json_writer');
const Aggregator = require('./aggregator');
const log = require('./logging');

log.info('Running ...');

const readStream = request('https://s3.eu-north-1.amazonaws.com/lemon-1/scooter_1337.csv');
const csvParser = csv({ headers: true, trim: true });
const errorFileWriter = new JSONFileWriter({ prefix: 'error_' });

readStream.pipe(csvParser)
  .pipe(new SchemaValidator())
  .on('invalid_record', (data) => {
    errorFileWriter.write(data);
  })
  .on('end', () => {
    errorFileWriter.end();
  })
  .pipe(new Aggregator())
  .pipe(new JSONFileWriter({ prefix: 'output_' }));
