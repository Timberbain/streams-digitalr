const NodeCache = require('node-cache');

class Cache {
  constructor(ttl, checkperiod) {
    this.cache = new NodeCache({ stdTTL: ttl, checkperiod });
  }

  set(key, val) {
    return new Promise((resolve, reject) => {
      this.cache.set(key, val, (err) => {
        if (err) {
          reject(err);
        } else {
          resolve(val);
        }
      });
    });
  }

  get(key) {
    return new Promise((resolve, reject) => {
      this.cache.get(key, (err, val) => {
        if (err) {
          reject(err);
        } else {
          resolve(val);
        }
      });
    });
  }
}

module.exports = Cache;
