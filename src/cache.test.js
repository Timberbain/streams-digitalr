const Cache = require('./cache');

describe('Cache', () => {
  it('get() without set should return undefined', async () => {
    const cache = new Cache(10, 10);
    expect(await cache.get('test')).toBe(undefined);
  });

  it('set() should return set value', async () => {
    const cache = new Cache(10, 10);
    expect(await cache.set('test', '123')).toBe('123');
  });

  it('get() after set() should return set value', async () => {
    const cache = new Cache(10, 10);
    await cache.set('test', '123');
    expect(await cache.get('test')).toBe('123');
  });

  it('get() after set() should not return different value than set', async () => {
    const cache = new Cache(10, 10);
    await cache.set('test', '123');
    expect(await cache.get('test')).not.toBe('456');
  });
});
