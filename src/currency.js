const cc = require('currency-codes');

module.exports = {
  formatToISO4217: (amount, currency) => {
    const info = cc.code(currency.toUpperCase());
    return amount * (10 ** info.digits);
  },

  formatISO4217ToDecimals: (amount, currency) => {
    const info = cc.code(currency.toUpperCase());
    return amount / (10 ** info.digits);
  },
};
