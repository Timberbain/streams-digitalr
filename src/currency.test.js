const {
  formatToISO4217,
  formatISO4217ToDecimals,
} = require('./currency');

describe('formatToISO4217()', () => {
  it('13.3 USD => 1330', () => {
    expect(formatToISO4217(13.37, 'USD')).toBe(1337);
  });
  it('13.37 SEK => 1337', () => {
    expect(formatToISO4217(13.37, 'SEK')).toBe(1337);
  });
});

describe('formatISO4217ToDecimals()', () => {
  it('1337 USD => 13.37', () => {
    expect(formatISO4217ToDecimals(1337, 'USD')).toBe(13.37);
  });
  it('1330 SEK => 13.3', () => {
    expect(formatISO4217ToDecimals(1330, 'SEK')).toBe(13.3);
  });
});
