const request = require('request-promise');

const Cache = require('./cache');

const cache = new Cache(10, 10);

module.exports = {
  async getRates(url = 'https://s3.eu-north-1.amazonaws.com/lemon-1/rate.json') {
    try {
      const cachedValue = await cache.get(url);
      if (cachedValue) return cachedValue;
      const rates = await request({ uri: url, method: 'GET', json: true });
      const dict = rates.reduce((o, v) => ({ ...o, ...{ [v.zone]: v } }), {});
      return cache.set(url, dict);
    } catch (e) {
      throw e;
    }
  },
};
