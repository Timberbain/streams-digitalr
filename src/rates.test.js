const {
  getRates,
} = require('./rates');

describe('getRates()', () => {
  it('Should return json object', async () => {
    const result = await getRates();
    expect(typeof result).toBe('object');
  });
  /* Add more test cases */
});
